import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MyServiceService } from '../my-service.service';
import { Response } from '@angular/http';
import { Product } from '../product';

@Component({
  selector: 'app-store-data',
  templateUrl: './store-data.component.html',
  styleUrls: ['./store-data.component.css']
})
export class StoreDataComponent implements OnInit{
  
  Data : Array<{category : string, product : string, MRP: number}> = [];
  AllData : Array<{Sl: number, category : string, product : string, MRP: number}> = [];
  length : number;
  quantity : number = 0;
  constructor(private serviceobj: MyServiceService, private router: Router) { }


  ngOnInit(){
      this.serviceobj.getData().subscribe(res => 
        { 
          this.length = res.length;
          console.log(this.length);
          for ( let i = 0; i < this.length; i++){ 
           
            this.AllData.push({
              Sl   : i+1,
              category : res[i].category,
              product : res[i].name,
              MRP : res[i].MRP,
          })}
          console.log(this.AllData);
        }),
        err => {
          console.log(err);
        }
}

AddToCart(sl) {
  for(let i = 0; i < this.AllData.length; i++) {
    if(this.AllData[i].Sl == sl ) {  
      this.Data.push ({
          category : this.AllData[i].category,
          product : this.AllData[i].product,
          MRP : this.AllData[i].MRP,
    })
  }
  }

  this.serviceobj.AddData(this.Data);
  this.Data.length = 0;

}


}

