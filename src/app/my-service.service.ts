import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Product } from './product';


@Injectable({
  providedIn: 'root'
})
export class MyServiceService {
  Product : Array<{category : string, product : string, MRP: number}> = [];
  constructor(private http: HttpClient) { }
  
  setData(data) {
    return this.http.post('https://angularcart-bb6ea.firebaseio.com/rahul.json', data)
  }
  getData(): Observable<any> {
    return this.http.get('https://angularcart-bb6ea.firebaseio.com/rahul.json').pipe(map(data =>data))
  }
  AddData(data) {
    for(let i = 0; i < data.length; i++) {
          this.Product.push ({
            category : data[i].category,
            product : data[i].product,
            MRP : data[i].MRP,
      })
    }
  }
  TakeData() {
    return this.Product;
  }
}