import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MyServiceService } from '../my-service.service';
import { Response } from '@angular/http';
import { Product } from '../product';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  Data: Array<{ category: string }> = [];
  length: number;
  length2: number = 0;
  fruit : Array<{category : string, product : string, MRP: number}> = [];
  vegetable : Array<{category : string, product : string, MRP: number}> = [];
   dairy : Array<{category : string, product : string, MRP: number}> = [];
   biscuit : Array<{category : string, product : string, MRP: number}> = [];
  constructor(private serviceobj: MyServiceService, private route: Router) { }

  ngOnInit() {
    this.serviceobj.getData().subscribe(res => {
      this.length = res.length;
      this.Data.push({
        category: res[0].category,
      })
      this.length2 = this.Data.length;

      for (var i = 1; i < this.length; i++) {
        for (var j = 0; j < this.length2; j++) {
          if (res[i].category == this.Data[j].category) {
            this.Data.push({
              category: res[i].category,
            })
            console.log(this.Data.length)
          }
        }
      }

      console.log("In header")
      console.log(this.Data);
    }),
      err => {
        console.log(err);
      }
  }

  Fruits() {
    this.serviceobj.getData().subscribe(res => {
      this.length = res.length;

      for (var i = 1; i < this.length; i++) {
          if (res[i].category == "fruit") {
            this.fruit.push({
              category : res[i].category,
              product : res[i].name,
              MRP : res[i].MRP,
            })
            console.log(this.Data.length)
          }
        }
        localStorage.setItem("Key", JSON.stringify(this.fruit));
        this.serviceobj.CallOtherComponent();
      }),
      err => {
        console.log(err);
      }
  }

}

