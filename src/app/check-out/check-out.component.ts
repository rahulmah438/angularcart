import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit {

  item = new Array();
  constructor() { }
  FinalData : Array<{Sl : number, category : string, product : string, MRP: number, Price: number}> = [];
  columns = ["Sl", "Product", "Category", "MRP", "Price"];
  Total : number = 0;
  ngOnInit() {
    this.item = (JSON.parse(localStorage.getItem("Key")));
    for(let i = 0; i < this.item.length; i++ ){
      this.FinalData.push({
        Sl   : i+1,
        category : this.item[i].category,
        product : this.item[i].product,
        MRP : this.item[i].MRP,
        Price : this.item[i].MRP,
    })
        this.Total += this.item[i].MRP;
  }
    // console.log(this.FinalData);
    }
    
  }

