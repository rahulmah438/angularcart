import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { StoreDataComponent } from './store-data/store-data.component';
import { HttpClientModule } from '@angular/common/http';
import { CheckOutComponent } from './check-out/check-out.component'
import { FormsModule } from '@angular/forms';



const route:Routes=[{
  path:   '',
  component:StoreDataComponent
},
{
  path:   'Checkout',
  component:CheckOutComponent
}
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    StoreDataComponent,
    CheckOutComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(route)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
