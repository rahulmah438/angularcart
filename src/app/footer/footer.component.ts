import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { MyServiceService } from '../my-service.service';
import { Routes,RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  items : Array<{category : string, product : string, MRP: number}> = [];
  columns = ["Product", "Category", "MRP"];

  constructor(private serviceobj: MyServiceService, private route : Router) { 
      this.items = this.serviceobj.TakeData();
 
 }
Checkout() {
  if(this.items.length > 0) {
    localStorage.setItem("Key", JSON.stringify(this.items));
    this.route.navigate(['/Checkout']);
  }
  else {
    alert("Your cart is empty");
  }
}
  
  

}
